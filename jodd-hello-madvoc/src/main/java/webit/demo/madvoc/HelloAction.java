// Copyright...
package webit.demo.madvoc;

import jodd.madvoc.meta.Action;
import jodd.madvoc.meta.MadvocAction;

/**
 *
 * @author zqq90
 */
@MadvocAction
public class HelloAction {

    @Action
    public Object view() {
        return "text:hello madvoc";
    }
}
